import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import bg from './assets/img/sky.jpg';
import { Parachute } from './components/Parachute';
import { Plane } from './components/Plane';
import { Bird } from './components/Bird';
import { Stars } from './components/Star';
import { Movement } from './components/Movement';
import Modal from './components/Modal';
import moment from 'moment/moment';


function App() {
  const [show, setShow] = useState(true);
  const [isPause, setIsPause] = useState(false);

  let canvas;
  let ctx;
  let maxBirdCount = 10;
  let lastBirdSpawnAt = Date.now();
  let maxParachuteCount = 10;
  let lastParachuteSpawnAt = Date.now();
  let maxStarCount = 10;
  let lastStarSpawnAt = Date.now();
  
  const plane = new Plane(950 / 2,550 / 1.5)
  const randomNumber = (min,max) => Math.random() * max + min;
  
  useEffect(() => {
    canvas = document.getElementById("myCanvas");

    document.addEventListener("keypress", (e) => {
      if(e.keyCode === 32){
        setIsPause(!isPause)
      }
    });
    
    if (!show) {
      let birds = [];
      let parachute = [];
      let moves = [];
      let star = [];
      let lengthPlaying = moment(Date.now()).format("ss")
      const move = (xpos, ypos) => moves.push(new Movement(xpos, ypos));
       const skyAngel = setInterval(() => {
          ctx = canvas.getContext("2d");
          ctx.clearRect(0, 0, 1024, 768);

          plane.update(move);
          plane.draw(ctx);

          const random = randomNumber(0, 700);
          const randomStar = randomNumber(0, 900);
          if (birds.length < maxBirdCount && Date.now() - lastBirdSpawnAt > 1500) {
            birds.push(new Bird(900, random, lengthPlaying));
            lastBirdSpawnAt = Date.now();
          }

          if (parachute.length < maxParachuteCount && Date.now() - lastParachuteSpawnAt > 1000) {
            parachute.push(new Parachute(random, -200));
            lastParachuteSpawnAt = Date.now();
          }

          if (star.length < maxStarCount && Date.now() - lastStarSpawnAt > 1200) {
            star.push(new Stars(randomStar, -200));
            lastStarSpawnAt = Date.now();
          }

          birds = birds.filter((enemy) => !enemy.dead);
          birds.forEach((Bird) => {
            Bird.update(plane, moves);
            Bird.draw(ctx);
          });

          parachute = parachute.filter((enemy) => !enemy.dead);
          parachute.forEach((parachute) => {
            parachute.update(plane, moves);
            parachute.draw(ctx);
          });

          star = star.filter((enemy) => !enemy.dead);
          star.forEach((star) => {
            star.update(plane, moves);
            star.draw(ctx);
          });

        }, 1000 / 30);
      

      setInterval(function () {
        plane.decreaseFuel();
      }, 1000);
    }
  })

  return (
    <div
    className='h-full w-full flex justify-center items-center bg-slate-400 flex-row'
    >
      {show && <Modal setIsOpen={setShow} />}

      <canvas
        id="myCanvas"
        width="1024"
        height="768"
        style={{
          backgroundImage: `url(${bg})`,
          backgroundSize: "cover",
          border: "2px solid #000000",
        }}
      >
        <script src='../index.js'></script>
      </canvas>
    </div>
  );
}

export default App;
