import moment from 'moment';
import img from '../assets/img/seagull.png'
export class Bird {
    speed = 4;
    dead = false;

    constructor(xPos,yPos, startTime){
        this.xPos = xPos;
        this.yPos = yPos;
        this.startTime = startTime;
    }

    isDead = () =>{
        if(this.xPos < -100 ){
            return true;
        }
    }

    update = (plane,bullets) => {
        if (this.dead) return;
        this.xPos -= this.speed;

        if(!this.dead && this.isDead()){
            this.dead = true;
        }
        
            if(!this.dead){
            if(Math.abs(plane.posX - this.xPos) < 50 && Math.abs(plane.posY - this.yPos) < 45){
                this.dead = true;
                let lastPlaying = moment(Date.now()).format("ss")
                console.log(this.startTime);
                console.log(lastPlaying);
                console.log(this.startTime - lastPlaying);
                plane.instantDead()
                }
            }
    
    }

    draw = (ctx) =>{
        const image = new Image();
        image.src = img;
        ctx.drawImage(image,this.xPos,this.yPos,50,50);
    }
}

export default Bird;