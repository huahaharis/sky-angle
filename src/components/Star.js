import img from '../assets/img/star.png'
export class Stars
 {
    speed = 5;
    dead = false;

    constructor(xPos,yPos){
        this.xPos = xPos;
        this.yPos = yPos;
    }

    isDead = () =>{
        if(this.yPos > 550){
            return true;
        }
    }

    update = (plane,bullets) => {
        if (this.dead) return;
        this.yPos += this.speed;

        if(!this.dead && this.isDead()){
            this.dead = true;
        }
        
            if(!this.dead){
            if(Math.abs(plane.posX - this.xPos) < 65 && Math.abs(plane.posY - this.yPos) < 90){
                this.dead = true;
                plane.increaseScore();
            }
            }
    
    }

    draw = (ctx) =>{
        const image = new Image();
        image.src = img;
        ctx.drawImage(image,this.xPos,this.yPos,75,100);
    }
}

export default Stars
;