import { useState } from 'react';
import img from '../assets/img/airplane.png'
export class Plane{
    dead = false;
    fuel = 10;
    ammo = 100;
    score = 0;
    speed = 25;
    firebullets = [];
    lastFireAt = Date.now();

    constructor(posX,posY){
        this.posX = posX;
        this.posY = posY;
    }

    increaseFuel = () => {
        this.fuel += 10;
        let audio = new Audio("/point.mp3");
        if (this.dead === false) {
          audio.play();
        }

    }

    decreaseFuel = () => {
        this.fuel -= 1
    }

    increaseScore = () =>{
        this.score += 10
        let audio = new Audio("/sfx_wing.mp3")
        audio.play()
    }

    instantDead = () =>{
        this.dead = true
        gameOver(this.score, this.fuel);
    }

    update = (firecb) =>{
        document.onkeydown = (e) =>{
            if(e.keyCode === 39){
                if (this.posX + this.speed <= 875) {
                this.posX += this.speed
                }
            }
            if(e.keyCode === 37){
                if (this.posX - this.speed >= 0) {
                    this.posX -= this.speed
                }
            }
            if(e.keyCode === 40){
                this.posY += this.speed
            }
            if(e.keyCode === 38){
                if (this.posY - this.speed > -10) {
                    this.posY -= this.speed
                }
            }
        }
        if (this.fuel <= 0) {
            this.dead = true;
            // let audio = new Audio("/die.mp3")
            // audio.play()
            gameOver(this.score);
        }
    }

    draw = (ctx) => {
        const image = new Image();
        image.src = img;
        ctx.drawImage(image,this.posX,this.posY,65,90);

        ctx.font = '16px Arial';
        ctx.fillStyle = "lightgreen";
        ctx.fillText(`Fuel: ${this.fuel}`, 15, 25);

        ctx.font = '16px Arial';
        ctx.fillStyle = "lightgreen";
        ctx.fillText(`Score: ${this.score}`, 15, 50);
    }
}


function gameOver(score, fuel) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [ firstName , setFirstName] = useState("");
    document.body.innerHTML = `
    <center>
    <br/>
    <h2>Game Over!</h2>
    <p>Your Score: ${score}</p>
    <button class="btn btn-danger mt-2" onClick="location.reload()">Again</button>
    <br/>
    <label for="fname">First name:</label>
    <input type="text" id="fname" name="fname"><br><br>
    </center>
    `
}

export default Plane;